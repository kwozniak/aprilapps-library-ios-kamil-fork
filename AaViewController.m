//
//  AaViewController.m
//  Transfer24
//
//  Created by Jacek Kwiecień on 24.10.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import "AaViewController.h"

@interface AaViewController ()

@end

@implementation AaViewController
@synthesize picker, pickerContainer, pickerHeightConstraint, popover;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];	
    pickerHeightConstraint.constant = 0;
    
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showCurtain{
    
    if (self.curtain) {
        [self.curtain removeFromSuperview];
        self.appDelegate.curtain = nil;
    }
    self.appDelegate.curtain = [IndicatorView addToViewControlle:self];
    self.curtain.frame = [[UIScreen mainScreen] bounds];
}

-(void)hideCurtain{
    
    [self.curtain removeFromSuperview];
    self.appDelegate.curtain = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.screenName = self.title;
}

-(void)closePicker
{
    [self onDismissPicker];
}

-(void)openPickerWithViewController:(AaBasePickerViewController *)createdPicker andSourceButton:(UIView *)button andHeight:(float)height
{
    [self openPickerWithViewController:createdPicker andSourceButton:button andHeight:height andDelegate:self];
}

-(void)openPickerWithViewController:(AaBasePickerViewController *)createdPicker andSourceButton:(UIView *)button andHeight:(float)height andDelegate:(id<AaPickerDelegate>)delegate
{
    //Pozwalam tylko na 1 popover/picker w jednym momencie
    if (picker) return;
    
    picker = createdPicker;
    picker.delegate = delegate;
    if (IS_IPAD) {
        popover = [[UIPopoverController alloc] initWithContentViewController:createdPicker];
        popover.delegate = self;
        popover.popoverContentSize = CGSizeMake(320, height);
        [popover presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections: UIPopoverArrowDirectionLeft | UIPopoverArrowDirectionUp animated:YES];
    } else {
        [self addChildViewController:picker];
        [pickerContainer addSubview:picker.view];
        [picker didMoveToParentViewController:self];
        
        pickerHeightConstraint.constant = height;
        [UIView animateWithDuration:0.4 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}




//Dodane ponieważ na IPadzie przy kliknieciu poza pickerem nie wywoływało
// onDismissPicker, i nie dało się wywołac na ekranie ponownie zadnego pickera
- (void) popoverControllerDidDismissPopover:(UIPopoverController *) popoverController
{
    [self onDismissPicker];
}


-(void)onDismissPicker
{
    if (picker && !popover) {
        pickerHeightConstraint.constant = 0;
        
        [UIView animateWithDuration:0.4 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished){
            if (finished) {
                [picker willMoveToParentViewController:nil];
                [picker.view removeFromSuperview];
                [picker removeFromParentViewController];
                picker = nil;
                [self onPickerDismissed];
            }            
        }];
    } else if (picker && popover) {
        [popover dismissPopoverAnimated:YES];
        popover = nil;
        picker = nil;
    }
}

-(void)onPickerDismissed
{
    //For override purposes
}


-(AppDelegate *)appDelegate
{
    return (AppDelegate *) [[UIApplication sharedApplication]delegate];
}

-(NSUserDefaults *)defaults
{
    return [NSUserDefaults standardUserDefaults];
}

-(IndicatorView *)curtain
{
    IndicatorView *curtain = self.appDelegate.curtain;
    return curtain;
}

@end
