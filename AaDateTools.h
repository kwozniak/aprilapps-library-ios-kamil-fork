//
//  AprilAppsDateTools.h
//  nPP Lux-Med
//
//  Created by Jacek Kwiecień on 03.07.2013.
//  Copyright (c) 2013 Lux-Med. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PATTERN_GOOGLE_API @"yyyy-MM-dd'T'HH:mm:ss.'000Z'"
#define PATTERN_HOUR_SHORT @"HH:mm"
#define PATTERN_HOUR_LONG @"HH:mm:ss"
#define PATTERN_DATE @"dd-MM-yyyy"

@interface AaDateTools : NSObject

+(NSDate*) addDays:(NSInteger)days toDate:(NSDate*)date;
+(NSDate*) addYears:(NSInteger)years toDate:(NSDate*)date;
+(NSDate*) minusDays:(NSInteger)days fromDate:(NSDate*)date;
+(NSInteger) daysBetweenDate:(NSDate*)dateFrom andDate:(NSDate*)dateTo;
+(NSDate*)addHours:(NSInteger)hoursToadd toDate:(NSDate*)date;
+(NSDate*) dateWithSpecifiedHour:(NSInteger)hour ofDay:(NSDate*)date;
+(NSDate*)addSeconds:(NSInteger)secondsToadd toDate:(NSDate*)date;
+(NSDate*)lastSecondOfADay:(NSDate*)date;
+(NSDate*) plusMinutes:(NSInteger)minutes toDate:(NSDate*)date;
+(NSDate*) minusMinutes:(NSInteger)minutes toDate:(NSDate*)date;

+(NSString *)stringFromDate:(NSDate *)date withPattern:(NSString*)pattern;
+(NSDate *)dateFromString:(NSString *)string withPattern:(NSString*)pattern;

+(NSMutableArray *)calendarCardWithDateInMonth:(NSDate *)date;
+(NSMutableArray *)calendarCardWithMonth:(NSInteger)month andYear:(NSInteger)year;

+(NSDate *)startOfTheMonthWithDateInMonth:(NSDate*)month;
+(NSDate *)startOfTheMonthWithMonth:(NSInteger)month andYear:(NSInteger)year;
+(NSDate *)endOfTheMonthWithDateInMonth:(NSDate*)month;
+(NSDate *)endOfTheMonthWithMonth:(NSInteger)month andYear:(NSInteger)year;
+(BOOL)isDateWithinCurrentMonth:(NSDate*)date;

+(BOOL) isDate:(NSDate*)date withinMonthFromDate:(NSDate*)withinMonthFromDate;
+(BOOL) isDate:(NSDate*)date withinMonth:(NSInteger)month andYear:(NSInteger)year;

+(NSDateComponents *)timeComponentsFromSeconds:(NSInteger)seconds;

+(NSArray*)monthsArrayFirstUpper;
@end
