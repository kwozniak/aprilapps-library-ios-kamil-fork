//
//  AaBasePickerViewController.m
//  Transfer24
//
//  Created by Jacek Kwiecień on 07.11.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import "AaBasePickerViewController.h"

@interface AaBasePickerViewController ()

@end

@implementation AaBasePickerViewController
@synthesize delegate, confirmButton, cancelButton, titleBar, navigationBar, tag;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (IBAction)confirmClicked:(UIBarButtonItem *)sender {
    [delegate onDismissPicker];
}

- (IBAction)cancelClicked:(UIBarButtonItem *)sender {
    [delegate onDismissPicker];
}

-(AppDelegate *)appDelegate
{
    return [[UIApplication sharedApplication] delegate];
}

@end
