//
//  AaDatePickerViewController.m
//  Transfer24
//
//  Created by Jacek Kwiecień on 07.11.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import "AaDatePickerViewController.h"

@interface AaDatePickerViewController ()

@end

@implementation AaDatePickerViewController
@synthesize datePicker, minDate, maxDate, startingDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (minDate) [self.datePicker setMinimumDate:minDate];
    if (maxDate) [self.datePicker setMaximumDate:maxDate];
    if (startingDate) [self.datePicker setDate:startingDate];
    else [self.datePicker setDate:[NSDate date]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    minDate = nil;
    maxDate = nil;
    startingDate = nil;
    datePicker = nil;
}

@end
