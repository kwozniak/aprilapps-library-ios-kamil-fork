//
//  AaBasePickerViewController.h
//  Transfer24
//
//  Created by Jacek Kwiecień on 07.11.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#define KEY_ANIMATION @"animation"
#define VALUE_HIDE_ANIMATION @"hide animation"
#define VALUE_SHOW_ANIMATION @"show animation"

@protocol AaPickerDelegate
@optional
- (void) onDismissPicker;
@end

@interface AaBasePickerViewController : UIViewController
{
    id __unsafe_unretained delegate;
    NSInteger tag;
}

@property (nonatomic) NSInteger tag;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *confirmButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleBar;
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (unsafe_unretained) id <AaPickerDelegate> delegate;

- (IBAction)confirmClicked:(UIBarButtonItem *)sender;
- (IBAction)cancelClicked:(UIBarButtonItem *)sender;
- (AppDelegate*)appDelegate;

@end
