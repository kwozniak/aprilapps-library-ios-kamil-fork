//
//  AaDatePickerViewController.h
//  Transfer24
//
//  Created by Jacek Kwiecień on 07.11.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AaBasePickerViewController.h"


@interface AaDatePickerViewController : AaBasePickerViewController
{
    UIDatePicker *datePicker;    
    NSDate *minDate;
    NSDate *maxDate;
    NSDate *startingDate;
}

@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) NSDate *minDate;
@property (nonatomic, retain) NSDate *maxDate;
@property (nonatomic, retain) NSDate *startingDate;

@end
