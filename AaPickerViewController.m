//
//  AaPickerViewController.m
//  Transfer24
//
//  Created by Jacek Kwiecień on 24.10.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import "AaPickerViewController.h"

@interface AaPickerViewController ()

@end

@implementation AaPickerViewController
@synthesize objects, selectedRow, picker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    objects = nil;
    picker = nil;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return objects.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    selectedRow = row;
}

-(UIView*) pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (!view) view = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 40)];
    [(UILabel*) view setText:[objects objectAtIndex:row]];
    return view;
}



@end
