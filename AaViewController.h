//
//  AaViewController.h
//  Transfer24
//
//  Created by Jacek Kwiecień on 24.10.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AaBasePickerViewController.h"
#import "AppDelegate.h" 
#import "AaAppUtils.h"
#import "AaDateTools.h"
#import "GAITrackedViewController.h"

@interface AaViewController : GAITrackedViewController <AaPickerDelegate, UIPopoverControllerDelegate>
{
    AaBasePickerViewController *picker;
}


@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) AaBasePickerViewController *picker;
@property (weak, nonatomic) IBOutlet UIView *pickerContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerHeightConstraint;

-(void) showCurtain;
-(void) hideCurtain;
//-(void) openPicker:(AaBasePickerViewController*)createdPicker withHeight:(float)height;
-(void) openPickerWithViewController:(AaBasePickerViewController*)createdPicker andSourceButton:(UIView*)button andHeight:(float)height;
-(void)openPickerWithViewController:(AaBasePickerViewController *)createdPicker andSourceButton:(UIView *)button andHeight:(float)height andDelegate:(id<AaPickerDelegate>)delegate;
-(void) closePicker;
-(AppDelegate*)appDelegate;
-(NSUserDefaults*)defaults;
-(void)onPickerDismissed;
-(IndicatorView*)curtain;
@end
